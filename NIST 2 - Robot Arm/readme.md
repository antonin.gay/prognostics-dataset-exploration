# NIST dataset 2 - Robot Arm

## Where to download the files for this directory ?

Data description can be found [here](https://data.nist.gov/od/id/754A77D9DA1E771AE0532457068179851962).
Data Headers can be found [there](https://www.nist.gov/document/ur5testresultheaderxlsx).

The data file used for the exploratory notebook, can be found
[there](https://www.nist.gov/document/ur5testresulthalfspeedpayload16lb1csv).
All the files can be found
[here](https://www.nist.gov/el/intelligent-systems-division-73500/degradation-measurement-robot-arm-position-accuracy),
with an augmented description of the dataset.

The downloaded files should be placed in the current folder for the notebook to run correctly.

Finally, [(Qiao and Weiss 2017)](https://tsapps.nist.gov/publication/get_pdf.cfm?pub_id=922364z) presents the dataset
and the _diagnostic_ work achieved by the authors.

Written by Antonin Gay <antonin.gay@arcelormittal.com>

