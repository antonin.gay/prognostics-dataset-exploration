# NIST dataset 1 - Linear Axis Rail

This dataset has been excluded from the study before any test, hence no data has been downloaded.

Although, they are available with [this link](https://data.nist.gov/od/id/6EF435207EF17114E0532457068155831934).
