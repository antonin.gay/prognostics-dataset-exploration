# NIST dataset 3 - Process and robot data from a two robot workcell

This dataset has been excluded from the study before any test, hence no data has been downloaded.

Although, they are available with [this link](https://data.nist.gov/od/id/mds2-2361).
