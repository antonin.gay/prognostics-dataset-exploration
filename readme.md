# DataSet Exploration for prognostics


This project explores public datasets for prognostics. It tends to find alternatives to C-MAPSS. The main criteria are:

- Run-to-failure trajectories
- ...

## Next dataset to explore

### NASA PCoE :

[Algae Raceway Data Set] 

[CFRP Composites Data Set] 

[Milling Data Set ] 

[Bearing Data Set]

[Battery Data Set] 

[Turbofan Engine Degradation Simulation Data Set] 

[PHM08 Challenge Data Set] 

[IGBT Accelerated Aging Data Set] 

[Trebuchet Data Set] 

[FEMTO Bearing Data Set] 

[Randomized Battery Usage Data Set] 

[Capacitor Electrical Stress Data Set] 

[MOSFET Thermal Overstress Aging Data Set] 

[Capacitor Electrical Stress Data Set - 2] 

[HIRF Battery Data Set] 

[Small Satellite Power Simulation Data Set]

[Turbofan Engine Degradation Simulation Data Set-2]

[Fatigue Crack Growth in Aluminum Lap Joint]

### Kaggle challenges

... To complete once listed

### Other

PHM 2018 -> A priori données trop longues. Traj run-to-failure. ~199 traj d'apprentissage ~39 test

Bearings by

[1] Wang B, Lei Y, Li N, Li N, A hybrid prognostics approach for estimating RUL of rolling elements bearings. IEEE Trans Reliab 2018; 1-12

